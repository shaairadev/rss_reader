package com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks

interface RefreshCallback {
    fun onRefresh()
}