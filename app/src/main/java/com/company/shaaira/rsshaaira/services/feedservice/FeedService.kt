package com.company.shaaira.rsshaaira.services.feedservice

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.company.shaaira.rsshaaira.feeddata.ParsedChannel
import com.company.shaaira.rsshaaira.services.feedservice.callbacks.DataDownloadedCallback
import com.company.shaaira.rsshaaira.services.feedservice.callbacks.ParsingDoneCallback
import com.company.shaaira.rsshaaira.services.feedservice.datamodules.RSSFeedParser
import com.company.shaaira.rsshaaira.services.feedservice.datamodules.XMLDownloader
import java.net.MalformedURLException
import java.net.URL
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

class RSSService : Service() {
    private val parsedChannels = HashMap<String, ParsedChannel?>()
    private val binder: IBinder = RSSBinder()
    private val queuedChannels = ArrayList<String>()
    private val downloadThreads = ArrayList<Thread>()

    override fun onDestroy() {
        super.onDestroy()
        for (thread in downloadThreads) {
            thread.interrupt()
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    fun fetchRSSFeed(urlString: String) {
        LOGGER.log(Level.INFO, "Service is starting fetching the RSS")
        val url = parseURL(urlString)
        if (url == null) {
            LOGGER.log(Level.WARNING, "Null url has been caught. Aborting connection attempt")
            broadcastDownloadComplete("")
            return
        }
        if (queuedChannels.contains(urlString)) {
            LOGGER.log(Level.WARNING, "Channel is already downloading. Aborting connection attempt")
            broadcastDownloadComplete("")
            return
        }
        queuedChannels.add(urlString)
        val downloadThread = Thread(Runnable {
            val xmlDownloader = XMLDownloader()
            xmlDownloader.downloadRSSFeed(url, object : DataDownloadedCallback {
                override fun onDownload(success: Boolean) {
                    LOGGER.log(Level.INFO, "Received download callback. Connection success status: $success")
                    if (success) {
                        parseXML(xmlDownloader, url)
                    } else {
                        LOGGER.log(Level.WARNING, "Timeout on connection by HTTP")
                    }
                }
            })
        })
        downloadThreads.add(downloadThread)
        downloadThread.start()
    }

    private fun parseURL(urlString: String): URL? {
        if (urlString.contains("http://") || urlString.contains("https://")) {
            try {
                return URL(urlString)
            } catch (e: MalformedURLException) {
                LOGGER.log(Level.WARNING, "Attempt to establish wrong URL")
            }
        }
        return null
    }

    fun getParsedChannel(channelUrl: String?): ParsedChannel? {
        return parsedChannels[channelUrl]
    }

    fun removeParsedChannel(channelUrl: String?) {
        if (parsedChannels.containsKey(channelUrl)) {
            parsedChannels.remove(channelUrl)
            if (parsedChannels.size < 1) {
                stopSelf()
            }
        }
    }

    private fun parseXML(xmlDownloader: XMLDownloader, rssUrl: URL) {
        val parseThread = Thread(Runnable {
            val parser = RSSFeedParser()
            parser.parseXML(rssUrl, xmlDownloader.rssDownloadResult, object : ParsingDoneCallback {
                override fun onParsingDone() {
                    try {
                        LOGGER.log(Level.INFO, "Putting new channel in map using Key: "
                                + parser.feedChannel.rssLink.toString())
                        parsedChannels[parser.feedChannel.rssLink.toString()] =
                                ParsedChannel(
                                        parser.feedChannel,
                                        parser.feedItems,
                                        parser.feedChannel.rssLink
                                )
                        LOGGER.log(Level.INFO, "Parsing complete. Broadcasting the message")
                        broadcastDownloadComplete(parser.feedChannel.title)
                        queuedChannels.remove(rssUrl.toString())
                    } finally {
                        xmlDownloader.closeDownloadStream()
                    }
                }
            })
        })
        parseThread.start()
    }

    private fun broadcastDownloadComplete(channelName: String?) {
        if (channelName == null) {
            LOGGER.log(Level.WARNING, "Empty channel name for a broadcast")
            return
        }
        val intent = Intent(actionFilter)
        intent.putExtra(intentTag, channelName)
        sendBroadcast(intent)
    }

    inner class RSSBinder : Binder() {
        val service: RSSService
            get() = this@RSSService
    }

    companion object {
        private val LOGGER = Logger.getLogger(RSSService::class.java.name)
        const val intentTag = "channelIntentTag"
        const val actionFilter = "com.company.shaaira.rsshaaira.DOWNLOAD_ACTION"

        fun makeIntent(context: Context?): Intent {
            return Intent(context, RSSService::class.java)
        }

    }
}