package com.company.shaaira.rsshaaira.screens

import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.ActivityResultListener
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.ActivityToolbarListener
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.ContextMenuListener
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.LifecycleListener
import java.util.*

abstract class BaseLifecycleActivity : AppCompatActivity() {

    protected val lifeCycleDependants = ArrayList<LifecycleListener>()

    protected val activityToolbarListeners = ArrayList<ActivityToolbarListener>()

    protected val activityResultListeners = ArrayList<ActivityResultListener>()

    protected val contextMenuListeners = ArrayList<ContextMenuListener>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        for (listener in lifeCycleDependants) {
            listener.onCreate(savedInstanceState)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        for (listener in lifeCycleDependants) {
            listener.onDestroy()
        }
    }

    override fun onStart() {
        super.onStart()
        for (listener in lifeCycleDependants) {
            listener.onStart()
        }
    }

    override fun onStop() {
        super.onStop()
        for (listener in lifeCycleDependants) {
            listener.onStop()
        }
    }

    override fun onResume() {
        super.onResume()
        for (listener in lifeCycleDependants) {
            listener.onResume()
        }
    }

    override fun onPause() {
        super.onPause()
        for (listener in lifeCycleDependants) {
            listener.onPause()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        for (listener in activityToolbarListeners) {
            listener.onOptionsItemSelected(item)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (listener in activityResultListeners) {
            listener.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        for (listener in activityToolbarListeners) {
            listener.onCreateOptionsMenu(menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        for (listener in lifeCycleDependants) {
            listener.onSaveInstanceState(outState)
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, view: View, menuInfo: ContextMenuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo)
        for (listener in contextMenuListeners) {
            listener.onCreateContextMenu(menu, view, menuInfo)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        for (listener in contextMenuListeners) {
            listener.onContextItemSelected(item)
        }
        return super.onContextItemSelected(item)
    }

    override fun onContextMenuClosed(menu: Menu) {
        super.onContextMenuClosed(menu)
        for (listener in contextMenuListeners) {
            listener.onContextMenuClosed(menu)
        }
    }
}