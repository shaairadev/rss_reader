package com.company.shaaira.rsshaaira.database

import android.provider.BaseColumns

object FeedEntryInfo : BaseColumns {
    const val TABLE_NAME = "entry"
    const val COLUMN_TITLE = "title"
    const val COLUMN_DESCRIPTION = "subtitle"
    const val COLUMN_AUTHOR = "author"
    const val COLUMN_GUID = "guid"
    const val COLUMN_PUBDATE = "pubDate"
    const val COLUMN_BITMAP = "bitmap"
    const val COLUMN_MEDIA_LINK = "mediaLink"
    const val COLUMN_SOURCE_LINK = "sourceLink"
    const val COLUMN_CHANNEL = "channel"
}