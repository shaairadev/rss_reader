package com.company.shaaira.rsshaaira.activitycallbackinterfaces

import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.Menu
import android.view.MenuItem
import android.view.View

interface ContextMenuListener {
    fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo)
    fun onContextItemSelected(item: MenuItem): Boolean
    fun onContextMenuClosed(menu: Menu)
}