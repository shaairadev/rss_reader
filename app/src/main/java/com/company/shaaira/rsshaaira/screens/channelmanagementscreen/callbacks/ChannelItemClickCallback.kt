package com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks

import java.net.URL

interface ChannelItemClickCallback {
    fun onClick(itemTitle: String?, url: URL?)
}