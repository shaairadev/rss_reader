package com.company.shaaira.rsshaaira.activitycallbackinterfaces

import android.os.Bundle

abstract class DefaultLifecycleListener : LifecycleListener {
    override fun onCreate(savedInstanceState: Bundle?) {}
    override fun onPause() {}
    override fun onStop() {}
    override fun onStart() {}
    override fun onResume() {}
    override fun onSaveInstanceState(outState: Bundle?) {}
    override fun onDestroy() {}
}