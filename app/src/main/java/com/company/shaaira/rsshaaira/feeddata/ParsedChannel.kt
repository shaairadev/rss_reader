package com.company.shaaira.rsshaaira.feeddata

import java.net.URL
import java.util.*

class ParsedChannel(
        val feedChannel: FeedChannel,
        val feedItems: ArrayList<FeedItem>,
        val rssLink: URL
)