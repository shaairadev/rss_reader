package com.company.shaaira.rsshaaira.feeddata


import java.net.URL

data class FeedChannel(
        val title: String,
        val shownName: String,
        val sourceLink: String,
        val description: String,
        val language: String,
        val rssLink: URL
)