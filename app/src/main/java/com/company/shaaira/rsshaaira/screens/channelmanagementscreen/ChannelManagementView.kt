package com.company.shaaira.rsshaaira.screens.channelmanagementscreen

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.company.shaaira.rsshaaira.R
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.ActivityToolbarListener
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.ContextMenuListener
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.DefaultLifecycleListener
import com.company.shaaira.rsshaaira.feeddata.FeedChannel
import com.company.shaaira.rsshaaira.screens.channelmanagementscreen.adapters.ChannelAdapter
import com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks.*
import java.net.URL
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

class ChannelManagementView(private val channelManagementActivity: AppCompatActivity) : DefaultLifecycleListener(), ActivityToolbarListener, ContextMenuListener {
    private lateinit var channelAdapter: ChannelAdapter
    private val cardTitleMap: HashMap<View?, String?> = HashMap()
    private lateinit var recyclerView: RecyclerView
    private var currentContextedChannel: String? = null


    private var channelClickCallback: ChannelItemClickCallback? = null
    private var channelDeleteCallback: ChannelDeleteCallback? = null
    private var channelEditCallback: ChannelEditCallback? = null
    private var addChannelClickCallback: AddChannelClickCallback? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            val savedChannelName = savedInstanceState.getString(CONTEXTED_CHANNEL_TAG)
            if (savedChannelName != null && "" != savedChannelName) {
                initRecyclerView()
                setCurrentContextedChannel(savedChannelName)
            }
        }
        val topToolbar = channelManagementActivity.findViewById<Toolbar>(R.id.channel_view_menu)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            topToolbar.setTitle(R.string.channels)
        }
        channelManagementActivity.setSupportActionBar(topToolbar)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        if ("" != currentContextedChannel) {
            outState!!.putString(CONTEXTED_CHANNEL_TAG, currentContextedChannel)
        }
    }

    private fun initRecyclerView(): ChannelAdapter {
        recyclerView = channelManagementActivity.findViewById(R.id.channel_recycler_view)
        val adapter = ChannelAdapter(ArrayList(), object : ChannelItemClickCallback {
            override fun onClick(itemTitle: String?, url: URL?) {
                channelClickCallback!!.onClick(itemTitle, url)
            }
        }, object : ChannelItemBoundCallback {
            override fun onItemBound(view: View?, title: String?) {
                channelManagementActivity.registerForContextMenu(view)
                cardTitleMap[view] = title
            }
        })

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(channelManagementActivity)
        return adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (R.id.menu_add_channel == item.itemId) {
            if (addChannelClickCallback != null) {
                addChannelClickCallback!!.onClick()
            }
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        inflateToolbarMenu(menu)
        return true
    }

    private fun inflateToolbarMenu(menu: Menu) {
        channelManagementActivity.menuInflater.inflate(R.menu.channels_toolbar_menu, menu)
        LOGGER.log(Level.INFO, "Inflating the menu for channels")
    }

    private fun setCurrentContextedChannel(title: String?) {
        if (title != null) {
            currentContextedChannel = title
        }
    }

    fun updateRecyclerView(feedChannels: ArrayList<FeedChannel>) {
        channelAdapter.updateAdapterData(feedChannels)
    }

    fun updateRecyclerView(feedChannel: FeedChannel) {
        channelAdapter.updateAdapterData(feedChannel)
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo) {
        channelManagementActivity.menuInflater.inflate(R.menu.channels_context_menu, menu)
        currentContextedChannel = cardTitleMap[v]
        LOGGER.log(Level.INFO, "Inflating context menu for a channel: $currentContextedChannel")
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_delete -> channelDeleteCallback!!.onDelete(currentContextedChannel)
            R.id.menu_item_edit -> channelEditCallback!!.onEdit(currentContextedChannel)
        }
        setCurrentContextedChannel("")
        return true
    }

    override fun onContextMenuClosed(menu: Menu) {
        setCurrentContextedChannel("")
    }

    val context: Context
        get() = channelManagementActivity

    fun setChannelClickCallback(feedItemClickCallback: ChannelItemClickCallback?) {
        channelClickCallback = feedItemClickCallback
    }

    fun setChannelDeleteCallback(channelDeleteCallback: ChannelDeleteCallback?) {
        this.channelDeleteCallback = channelDeleteCallback
    }

    fun setAddChannelClickCallback(addChannelClickCallback: AddChannelClickCallback?) {
        this.addChannelClickCallback = addChannelClickCallback
    }

    fun setChannelEditCallback(channelEditCallback: ChannelEditCallback?) {
        this.channelEditCallback = channelEditCallback
    }

    companion object {
        private val LOGGER = Logger.getLogger(ChannelManagementView::class.java.name)
        private const val CONTEXTED_CHANNEL_TAG = "context_tag_channel"
    }
}