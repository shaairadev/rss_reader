package com.company.shaaira.rsshaaira.scheduleupdater

import android.content.*
import android.os.IBinder
import com.company.shaaira.rsshaaira.broadcasters.BroadcastPasser
import com.company.shaaira.rsshaaira.broadcasters.ReceiveListener
import com.company.shaaira.rsshaaira.services.feedservice.RSSService
import com.company.shaaira.rsshaaira.services.feedservice.RSSService.RSSBinder
import com.company.shaaira.rsshaaira.services.feedservice.ServiceHandler
import com.company.shaaira.rsshaaira.services.feedservice.datamodules.DatabaseStorageManager
import java.util.logging.Level
import java.util.logging.Logger

class AlarmReceiver : BroadcastReceiver() {
    private var databaseStorageManager: DatabaseStorageManager? = null
    private var broadcastPasser: BroadcastPasser? = null
    private var serviceHandler: ServiceHandler? = null
    private var receiveListener: ReceiveListener? = null
    private var serviceBound = false
    private var rssService: RSSService? = null
    private var currentChannelUrl: String = ""
    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            val binder = service as RSSBinder
            rssService = binder.service
            rssService?.fetchRSSFeed(currentChannelUrl)
        }

        override fun onServiceDisconnected(arg0: ComponentName) {}
    }

    override fun onReceive(context: Context, intent: Intent) {
        currentChannelUrl = intent.getStringExtra(URL_INTENT_ID) ?: ""
        if (!serviceBound) {
            serviceHandler!!.bindRSSService(context, connection)
            serviceBound = true
        }
        databaseStorageManager = DatabaseStorageManager(context)
        broadcastPasser = BroadcastPasser()
        serviceHandler = ServiceHandler()
        startReceiver(context)
    }

    private fun startReceiver(context: Context) {
        val intentFilter = IntentFilter()
        intentFilter.addAction(RSSService.actionFilter)
        receiveListener = makeReceiveListener(context)
        broadcastPasser!!.addBroadcastListener(receiveListener!!)
        context.registerReceiver(broadcastPasser, intentFilter)
        LOGGER.log(Level.INFO, "Feed receiver has been registered")
    }

    private fun stopReceiver(context: Context) {
        broadcastPasser!!.removeBroadcastListener(receiveListener!!)
        context.unregisterReceiver(broadcastPasser)
        LOGGER.log(Level.INFO, "Feed receiver has been unregistered")
    }

    private fun makeReceiveListener(listeningContext: Context): ReceiveListener {
        return object : ReceiveListener {
            override fun onReceiveCallback(context: Context, intent: Intent) {
                LOGGER.log(Level.INFO, "Working with callback of broadcast received from schedule update")
                LOGGER.log(Level.INFO, "Intent was not empty, proceeding work on broadcast")
                val channel = rssService!!.getParsedChannel(currentChannelUrl)
                LOGGER.log(Level.INFO, "Background updated channel url: $currentChannelUrl")
                if (channel != null) {
                    LOGGER.log(Level.INFO, "Service has returned scheduled update for a channel")
                    databaseStorageManager!!.saveFeedData(channel.feedItems)
                    stopReceiver(listeningContext)
                    serviceHandler!!.unbindRSSService(listeningContext, connection)
                } else {
                    LOGGER.log(Level.FINE, "Getting null channel")
                }
            }
        }
    }

    companion object {
        private const val URL_INTENT_ID = "alarm_channel_url"
        private val LOGGER = Logger.getLogger(AlarmReceiver::class.java.name)
        @JvmStatic
        fun makeAlarmIntent(context: Context?, channelUrl: String?): Intent {
            val alarmIntent = Intent(context, AlarmReceiver::class.java)
            alarmIntent.putExtra(URL_INTENT_ID, channelUrl)
            return alarmIntent
        }
    }
}