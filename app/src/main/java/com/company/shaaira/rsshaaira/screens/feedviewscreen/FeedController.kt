package com.company.shaaira.rsshaaira.screens.feedviewscreen

import android.app.Activity
import android.content.*
import android.os.Bundle
import android.os.IBinder
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.ActivityResultListener
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.DefaultLifecycleListener
import com.company.shaaira.rsshaaira.broadcasters.BroadcastPasser
import com.company.shaaira.rsshaaira.broadcasters.ReceiveListener
import com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks.ChannelMenuCallback
import com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks.FeedItemClickCallback
import com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks.RefreshCallback
import com.company.shaaira.rsshaaira.screens.feedviewscreen.feedcontrollermodules.FeedViewListener
import com.company.shaaira.rsshaaira.services.feedservice.RSSService
import com.company.shaaira.rsshaaira.services.feedservice.RSSService.RSSBinder
import com.company.shaaira.rsshaaira.services.feedservice.ServiceHandler
import com.company.shaaira.rsshaaira.services.feedservice.callbacks.RSSReadyCallback
import java.util.logging.Level
import java.util.logging.Logger

class FeedController internal constructor(private val feedView: FeedView) : DefaultLifecycleListener(), ActivityResultListener {
    private val feedModel: FeedModel = FeedModel(feedView.context)
    private val feedViewListener: FeedViewListener = FeedViewListener()
    private val broadcastPasser: BroadcastPasser = BroadcastPasser()
    private val serviceHandler: ServiceHandler = ServiceHandler()
    private var receiveListener: ReceiveListener? = null
    private var serviceBound = false
    private var rssService: RSSService? = null
    private var currentChannelName: String? = null
    private var currentChannelUrl: String? = null
    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            val binder = service as RSSBinder
            rssService = binder.service
            LOGGER.log(Level.INFO, "RSS service has been bound")
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            LOGGER.log(Level.INFO, "RSS service has been unbound")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            currentChannelName = savedInstanceState.getString(CURRENT_CHANNEL_NAME, "")
            currentChannelUrl = savedInstanceState.getString(CURRENT_CHANNEL_URL, "")
            LOGGER.log(Level.INFO, "Current channel url from bundle: $currentChannelUrl")
        } else {
            val sharedPreferences = context.getSharedPreferences(CURRENT_CHANNEL_TAG, Context.MODE_PRIVATE)
            currentChannelName = sharedPreferences.getString(CURRENT_CHANNEL_NAME, "")
            currentChannelUrl = sharedPreferences.getString(CURRENT_CHANNEL_URL, "")
            LOGGER.log(Level.INFO, "Current channel url from prefs: $currentChannelUrl")
        }
        serviceHandler.startService(context, RSS_SERVICE_PATH)
        loadSavedData()
    }

    override fun onStart() {
        if (!serviceBound) {
            serviceHandler.bindRSSService(context, connection)
            serviceBound = true
        }
        feedModel.instantiateDatabase()
    }

    override fun onStop() {
        if (serviceBound) {
            serviceHandler.unbindRSSService(context, connection)
            serviceBound = false
        }
        feedModel.closeDatabase()
        val sharedPreferences = context.getSharedPreferences(CURRENT_CHANNEL_TAG, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(CURRENT_CHANNEL_NAME, currentChannelName)
        editor.putString(CURRENT_CHANNEL_URL, currentChannelUrl)
        editor.apply()
    }

    override fun onResume() {
        startReceiver()
    }

    override fun onPause() {
        stopReceiver()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        LOGGER.log(Level.INFO, "Saving current channel url to instance: $currentChannelUrl")
        outState!!.putString(CURRENT_CHANNEL_NAME, currentChannelName)
        outState.putString(CURRENT_CHANNEL_URL, currentChannelUrl)
    }

    private val context: Context
        get() = feedView.context

    private fun loadSavedData() {
        feedView.setToolbarTitle(currentChannelName)
        if ("" == currentChannelName) {
            return
        }
        feedView.setRefreshAnimation(true)
        feedModel.loadFromDatabase(currentChannelName!!, object : RSSReadyCallback {
            override fun onReady(result: Boolean) {
                if (result) {
                    feedView.updateRecyclerView(feedModel.getFeed(currentChannelName)!!)
                } else {
                    LOGGER.log(Level.WARNING, "Error on getting data from database storage")
                    onRefreshCallback()
                }
            }
        })
    }

    private fun onRefreshCallback() {
        LOGGER.log(Level.INFO, "Received callback for channel refresh")
        if (!serviceBound || rssService == null) {
            feedView.setRefreshAnimation(false)
            LOGGER.log(Level.WARNING, "Attempt to get RSS from unbound service")
            return
        }
        rssService!!.fetchRSSFeed(currentChannelUrl!!)
    }

    private fun startReceiver() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(RSSService.actionFilter)
        receiveListener = makeReceiveListener()
        broadcastPasser.addBroadcastListener(receiveListener!!)
        context.registerReceiver(broadcastPasser, intentFilter)
        LOGGER.log(Level.INFO, "Feed receiver has been registered")
    }

    private fun stopReceiver() {
        broadcastPasser.removeBroadcastListener(receiveListener!!)
        context.unregisterReceiver(broadcastPasser)
        LOGGER.log(Level.INFO, "Feed receiver has been unregistered")
    }

    private fun makeReceiveListener(): ReceiveListener {
        return object : ReceiveListener {
            override fun onReceiveCallback(context: Context, intent: Intent) {
                LOGGER.log(Level.INFO, "Working with callback of broadcast received")
                LOGGER.log(Level.INFO, "Intent was not empty, proceeding work on broadcast")
                val channelName = intent.getStringExtra(RSSService.intentTag) ?: ""
                val channel = rssService!!.getParsedChannel(currentChannelUrl)
                LOGGER.log(Level.INFO, "Current channel url: $currentChannelUrl")
                if (channel != null) {
                    LOGGER.log(Level.INFO, "Service has returned channel data")
                    feedModel.addNewChannelItems(channelName, channel.feedItems)
                    feedModel.saveToDatabase(channelName)
                    if (channelName == currentChannelName) {
                        feedView.updateRecyclerView(channel.feedItems)
                    }
                    rssService!!.removeParsedChannel(currentChannelUrl)
                } else {
                    feedView.setRefreshAnimation(false)
                    LOGGER.log(Level.FINE, "Getting null channel. Stopping the refresh")
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        LOGGER.log(Level.INFO, "Getting activity result callback")
        if (requestCode == REQUEST_CODE) {
            LOGGER.log(Level.INFO, "Request code success: $requestCode")
            if (resultCode == Activity.RESULT_OK) {
                LOGGER.log(Level.INFO, "Result code success: RESULT_OK")
                val sharedPreferences = context.getSharedPreferences(CURRENT_CHANNEL_TAG, Context.MODE_PRIVATE)
                val checkedChannelName = sharedPreferences.getString(CURRENT_CHANNEL_NAME, "")
                val checkedChannelUrl = sharedPreferences.getString(CURRENT_CHANNEL_URL, "")
                if (checkedChannelName == currentChannelName || checkedChannelUrl == currentChannelUrl) {
                    return
                }
                currentChannelUrl = checkedChannelUrl
                currentChannelName = checkedChannelName
                loadSavedData()
            }
        }
    }

    companion object {
        private val LOGGER = Logger.getLogger(FeedController::class.java.name)
        private const val CURRENT_CHANNEL_TAG = "shared_options_tag"
        private const val CURRENT_CHANNEL_NAME = "name_current_channel"
        private const val CURRENT_CHANNEL_URL = "url_current_channel"
        private const val RSS_SERVICE_PATH = "com.company.shaaira.rsshaaira.services.rss_service.RSSService"
        private const val REQUEST_CODE = 17762
    }

    init {
        feedView.setChannelMenuCallback(object : ChannelMenuCallback {
            override fun onClick() {
                feedViewListener.onChannelMenuCallback(context, REQUEST_CODE)
            }
        })
        feedView.setRefreshCallback(object : RefreshCallback {
            override fun onRefresh() {
                onRefreshCallback()
            }
        })
        feedView.setFeedClickCallback(object : FeedItemClickCallback {
            override fun onClick(itemData: String) {
                feedViewListener.onFeedClickCallback(itemData, context)
            }
        })
    }
}