package com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks

interface FeedItemClickCallback {
    fun onClick(itemData: String)
}