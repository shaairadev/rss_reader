package com.company.shaaira.rsshaaira.screens.channelmanagementscreen;

import com.company.shaaira.rsshaaira.feeddata.FeedChannel;
import com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks.ChannelDataReadCallback;
import com.company.shaaira.rsshaaira.services.feedservice.datamodules.InnerStorageDataManager;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import lombok.val;

final class ChannelManagementModel
{
    private static final Logger LOGGER = Logger.getLogger(InnerStorageDataManager.class.getName());
    private final InnerStorageDataManager innerStorageDataManager;
    private ArrayList<FeedChannel> feedChannels;

    ChannelManagementModel()
    {
        innerStorageDataManager = new InnerStorageDataManager();
        feedChannels = new ArrayList<>();
    }

    void loadChannelDataInternal(final FileInputStream fileInputStream, final ChannelDataReadCallback callback)
    {
        try
        {
            innerStorageDataManager.readSavedChannelData(fileInputStream, new ChannelDataReadCallback()
            {
                @Override
                public void onReady()
                {
                    if (feedChannels != null)
                    {
                        feedChannels = innerStorageDataManager.getFeedChannels();
                        callback.onReady();
                    }
                }
            });
        }
        catch (final IOException e)
        {
            LOGGER.log(Level.WARNING, "Error on reading file");
        }
        catch (final ClassNotFoundException e)
        {
            LOGGER.log(Level.WARNING, "Class " + FeedChannel.class.getName() + " was not found in file. Cast exception");
        }
    }

    void saveChannelDataInternal(final FileOutputStream fileOutputStream)
    {
        try
        {
            innerStorageDataManager.saveChannelData(fileOutputStream, feedChannels);
        }
        catch (IOException e)
        {
            LOGGER.log(Level.WARNING, "IO Exception on attempt to save channels data");
        }
    }

    boolean addNewChannel(final FileOutputStream fileOutputStream, FeedChannel newChannel)
    {
        for (val channel : feedChannels)
        {
            if (channel.getTitle().equals(newChannel.getTitle()))
            {
                return false;
            }
        }

        feedChannels.add(newChannel);
        saveChannelDataInternal(fileOutputStream);
        return true;
    }

    boolean deleteChannel(final FileOutputStream fileOutputStream, String channelTitle)
    {
        FeedChannel feedChannel = null;

        for (val channel : feedChannels)
        {
            if(channel.getTitle().equals(channelTitle))
            {
                feedChannel = channel;
                break;
            }
        }

        if(feedChannel == null)
        {
            return false;
        }
        feedChannels.remove(feedChannel);
        saveChannelDataInternal(fileOutputStream);
        return true;
    }

    boolean checkUrlTaken(String url)
    {
        for (FeedChannel channel : feedChannels)
        {
            if (channel.getRssLink().toString().equals(url))
            {
                return true;
            }
        }
        return false;
    }

    ArrayList<FeedChannel> getFeedChannels()
    {
        return feedChannels;
    }
}
