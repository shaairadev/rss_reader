package com.company.shaaira.rsshaaira.screens.webviewscreen


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.company.shaaira.rsshaaira.R
import java.util.logging.Level
import java.util.logging.Logger

class FeedWebViewActivity : AppCompatActivity() {
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        val webView = findViewById<View>(R.id.webView) as WebView
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        val urlString = this.intent.getStringExtra(URL_INTENT_ID)
        LOGGER.log(Level.INFO, "WebView Url string : $urlString")
        webView.loadUrl(urlString)
    }

    companion object {
        private const val URL_INTENT_ID = "web_view_url"
        private val LOGGER = Logger.getLogger(FeedWebViewActivity::class.java.name)

        fun makeStartIntent(context: Context, urlString: String): Intent {
            val urlIntent = Intent(context, FeedWebViewActivity::class.java)
            urlIntent.putExtra(URL_INTENT_ID, urlString)
            return urlIntent
        }
    }
}