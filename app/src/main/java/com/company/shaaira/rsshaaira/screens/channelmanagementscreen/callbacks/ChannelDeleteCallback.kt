package com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks

interface ChannelDeleteCallback {
    fun onDelete(itemTitle: String?)
}