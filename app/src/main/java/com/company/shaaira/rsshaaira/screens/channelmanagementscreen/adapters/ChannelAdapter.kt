package com.company.shaaira.rsshaaira.screens.channelmanagementscreen.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.company.shaaira.rsshaaira.R
import com.company.shaaira.rsshaaira.feeddata.FeedChannel
import com.company.shaaira.rsshaaira.screens.channelmanagementscreen.adapters.ChannelAdapter.FeedChannelViewHolder
import com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks.ChannelItemBoundCallback
import com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks.ChannelItemClickCallback
import java.util.*

class ChannelAdapter(
        private val feedChannels: ArrayList<FeedChannel>,
        private val channelClickCallback: ChannelItemClickCallback,
        private val channelItemBoundCallback: ChannelItemBoundCallback
) : RecyclerView.Adapter<FeedChannelViewHolder>() {


    fun updateAdapterData(feedChannels: ArrayList<FeedChannel>) {
        this.feedChannels.clear()
        this.feedChannels.addAll(feedChannels)
        notifyDataSetChanged()
    }

    fun updateAdapterData(feedChannel: FeedChannel) {
        feedChannels.add(feedChannel)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): FeedChannelViewHolder {
        return FeedChannelViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.channel_row, viewGroup, false))
    }

    override fun onBindViewHolder(
            channelViewHolder: FeedChannelViewHolder,
            position: Int) {
        val params = channelViewHolder.mInfoLayout.layoutParams
        channelViewHolder.mInfoLayout.layoutParams = params
        channelViewHolder.mTxtName.text = feedChannels[position].shownName
        channelViewHolder.mCardView.setContentPadding(PADDING_HORIZONTAL, PADDING_VERTICAL, PADDING_HORIZONTAL, PADDING_VERTICAL)
        channelViewHolder.mCardView.setOnClickListener { channelClickCallback.onClick(feedChannels[position].title, feedChannels[position].rssLink) }
        channelItemBoundCallback.onItemBound(channelViewHolder.mCardView, feedChannels[position].title)
        channelViewHolder.mTxtRefreshDate.visibility = View.VISIBLE
    }

    override fun getItemCount(): Int {
        return feedChannels.size
    }

    class FeedChannelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mTxtName: TextView = itemView.findViewById(R.id.channel_txtName)
        val mTxtRefreshDate: TextView = itemView.findViewById(R.id.channel_txtRefreshDate)
        val mCardView: CardView = itemView.findViewById(R.id.channel_card_parent_layout)
        val mInfoLayout: LinearLayout = itemView.findViewById(R.id.channel_infoLayout)

    }

    companion object {
        private const val PADDING_HORIZONTAL = 50
        private const val PADDING_VERTICAL = 10
    }

    //FIXME: Make notify on load of screen, not automatic
    init {
        notifyDataSetChanged()
    }
}