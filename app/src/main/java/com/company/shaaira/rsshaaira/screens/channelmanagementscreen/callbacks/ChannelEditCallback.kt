package com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks

interface ChannelEditCallback {
    fun onEdit(title: String?)
}