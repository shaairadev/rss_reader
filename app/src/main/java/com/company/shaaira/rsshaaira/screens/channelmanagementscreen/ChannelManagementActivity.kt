package com.company.shaaira.rsshaaira.screens.channelmanagementscreen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.company.shaaira.rsshaaira.R
import com.company.shaaira.rsshaaira.screens.BaseLifecycleActivity

class ChannelManagementActivity : BaseLifecycleActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_channel_managment)
        val channelManagementView = ChannelManagementView(this)
        val channelManagementController = ChannelManagementController(channelManagementView)
        lifeCycleDependants.add(channelManagementView)
        lifeCycleDependants.add(channelManagementController)
        activityToolbarListeners.add(channelManagementView)
        activityResultListeners.add(channelManagementController)
        contextMenuListeners.add(channelManagementView)
        super.onCreate(savedInstanceState)
    }

    companion object {
        @JvmStatic
        fun makeStartIntent(context: Context?): Intent {
            return Intent(context, ChannelManagementActivity::class.java)
        }
    }
}