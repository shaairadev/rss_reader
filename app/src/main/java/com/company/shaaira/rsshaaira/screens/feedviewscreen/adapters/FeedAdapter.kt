package com.company.shaaira.rsshaaira.screens.feedviewscreen.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.company.shaaira.rsshaaira.R
import com.company.shaaira.rsshaaira.feeddata.FeedItem
import com.company.shaaira.rsshaaira.screens.feedviewscreen.adapters.FeedAdapter.FeedViewHolder
import com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks.FeedItemClickCallback
import com.company.shaaira.rsshaaira.screens.feedviewscreen.feedcontrollermodules.FeedDateFormatter
import java.util.*

class FeedAdapter(
        private val feedItems: ArrayList<FeedItem>,
        private val feedClickCallback: FeedItemClickCallback
) : RecyclerView.Adapter<FeedViewHolder>() {

    private val feedDateFormatter: FeedDateFormatter = FeedDateFormatter()

    fun updateAdapterData(feedItems: ArrayList<FeedItem>) {
        this.feedItems.clear()
        this.feedItems.addAll(feedItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): FeedViewHolder {
        return FeedViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.rss_row, viewGroup, false))
    }

    override fun onBindViewHolder(feedViewHolder: FeedViewHolder, position: Int) {
        val params = feedViewHolder.infoLayout.layoutParams
        feedViewHolder.feedMedia.visibility = View.VISIBLE
        feedViewHolder.feedMedia.setImageBitmap(feedItems[position].bitmap)
        feedViewHolder.infoLayout.layoutParams = params
        feedViewHolder.txtTitle.text = feedItems[position].title
        val formattedDate = feedDateFormatter.getFormattedDate(feedItems[position].pubDate)
        feedViewHolder.txtPubDate.text = formattedDate
        if (feedItems[position].description == "") {
            feedViewHolder.txtContent.visibility = View.GONE
        }
        feedViewHolder.txtContent.text = feedItems[position].description
        feedViewHolder.cardView.setContentPadding(PADDING_HORIZONTAL, PADDING_VERTICAL, PADDING_HORIZONTAL, PADDING_VERTICAL)
        feedViewHolder.cardView.setOnClickListener { feedClickCallback.onClick(feedItems[position].sourceLink) }
    }

    override fun getItemCount(): Int {
        return feedItems.size
    }

    class FeedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtTitle: TextView = itemView.findViewById(R.id.feed_txtTitle)
        val txtPubDate: TextView = itemView.findViewById(R.id.feed_txtPubDate)
        val txtContent: TextView = itemView.findViewById(R.id.feed_description)
        val feedMedia: ImageView = itemView.findViewById(R.id.feed_media)
        val cardView: CardView = itemView.findViewById(R.id.feed_card_parent_layout)
        val infoLayout: LinearLayout = itemView.findViewById(R.id.feed_infoLayout)
    }

    companion object {
        private const val PADDING_HORIZONTAL = 50
        private const val PADDING_VERTICAL = 12
    }

    //FIXME: Update on adding data - not on create
    init {
        notifyDataSetChanged()
    }
}