package com.company.shaaira.rsshaaira.screens.feedviewscreen.feedcontrollermodules

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import com.company.shaaira.rsshaaira.screens.channelmanagementscreen.ChannelManagementActivity.Companion.makeStartIntent
import com.company.shaaira.rsshaaira.screens.webviewscreen.FeedWebViewActivity.Companion.makeStartIntent

class FeedViewListener {
    fun onChannelMenuCallback(context: Context, requestCode: Int) {
        val intent = makeStartIntent(context)
        (context as AppCompatActivity).startActivityForResult(intent, requestCode)
    }

    fun onFeedClickCallback(itemUrl: String?, context: Context) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(itemUrl))
        try {
            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {
            context.startActivity(makeStartIntent(context, itemUrl!!))
        }
    }
}