package com.company.shaaira.rsshaaira.services.feedservice.datamodules

import com.company.shaaira.rsshaaira.feeddata.FeedChannel
import com.company.shaaira.rsshaaira.feeddata.FeedItem
import com.company.shaaira.rsshaaira.feeddata.FeedItemSerializable
import com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks.ChannelDataReadCallback
import com.company.shaaira.rsshaaira.services.feedservice.callbacks.RSSReadyCallback
import java.io.*
import java.net.MalformedURLException
import java.util.logging.Level
import java.util.logging.Logger

class InnerStorageDataManager {
    private var mFeedItems: ArrayList<FeedItem> = ArrayList()
    private var mFeedChannels: ArrayList<FeedChannel> = ArrayList()

    fun saveFeedData(fileOutputStream: FileOutputStream, feedItems: ArrayList<FeedItem>) {
        val objectStream = ObjectOutputStream(fileOutputStream)
        val serialFeedItem = ArrayList<FeedItemSerializable>()
        LOGGER.log(Level.INFO, "Saving feed data to internal storage")
        for (item in feedItems) {
            serialFeedItem.add(FeedItemSerializable.serialize(item))
        }
        objectStream.writeObject(serialFeedItem)
        objectStream.close()
    }

    fun readSavedFeedData(fileInputStream: FileInputStream?, callback: RSSReadyCallback) {
        val objectStream: ObjectInputStream = try {
            ObjectInputStream(fileInputStream)
        } catch (e: IOException) {
            LOGGER.log(Level.WARNING, "Can't open the object stream")
            return
        }
        val serialFeedItems: ArrayList<FeedItemSerializable>
        serialFeedItems = try {
            objectStream.readObject() as ArrayList<FeedItemSerializable>
        } catch (e: IOException) {
            LOGGER.log(Level.WARNING, "Exception on attempt to read object from file")
            return
        } catch (e: ClassNotFoundException) {
            LOGGER.log(Level.WARNING, FeedItemSerializable::class.java.name + " was not found in project")
            return
        } finally {
            try {
                objectStream.close()
            } catch (e: IOException) {
                LOGGER.log(Level.WARNING, "Exception on closing the closed stream")
            }
        }
        mFeedItems = ArrayList()
        for (item in serialFeedItems) {
            try {
                mFeedItems.add(FeedItemSerializable.deserialize(item))
            } catch (e: MalformedURLException) {
                LOGGER.log(Level.WARNING, "Invalid URL in feed data, skipping feed item")
            }
        }
        try {
            objectStream.close()
        } catch (e: IOException) {
            LOGGER.log(Level.WARNING, "Exception on closing the closed stream")
        }
        callback.onReady(true)
    }

    val feedItems: ArrayList<FeedItem>?
        get() {
            LOGGER.log(Level.WARNING, "No feed items were read, attempt for access")
            return mFeedItems
        }

    @Throws(IOException::class)
    fun saveChannelData(fileOutputStream: FileOutputStream?, feedChannels: ArrayList<FeedChannel?>?) {
        val objectStream = ObjectOutputStream(fileOutputStream)
        LOGGER.log(Level.INFO, "Saving channel data to internal storage")
        objectStream.writeObject(feedChannels)
        objectStream.close()
    }

    @Throws(IOException::class, ClassNotFoundException::class)
    fun readSavedChannelData(fileInputStream: FileInputStream?, callback: ChannelDataReadCallback) {
        val objectStream = ObjectInputStream(fileInputStream)
        mFeedChannels = objectStream.readObject() as ArrayList<FeedChannel>
        objectStream.close()
        callback.onReady()
    }

    val feedChannels: ArrayList<FeedChannel>?
        get() {
            LOGGER.log(Level.WARNING, "No feed channels were read, attempt for access")
            return mFeedChannels
        }

    companion object {
        private val LOGGER = Logger.getLogger(InnerStorageDataManager::class.java.name)
    }
}