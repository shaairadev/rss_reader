package com.company.shaaira.rsshaaira.services.feedservice.datamodules

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.text.Html
import com.company.shaaira.rsshaaira.feeddata.FeedChannel
import com.company.shaaira.rsshaaira.feeddata.FeedItem
import com.company.shaaira.rsshaaira.services.feedservice.callbacks.ParsingDoneCallback
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.io.IOException
import java.io.InputStream
import java.net.MalformedURLException
import java.net.URL
import java.util.logging.Level
import java.util.logging.Logger

//TODO: needs a BIG refactoring
class RSSFeedParser {
    var feedItems: ArrayList<FeedItem> = ArrayList()
        private set
    var feedChannel: FeedChannel = FeedChannel("", "", "", "", "", URL(""))
        private set

    fun parseXML(rssUrl: URL?, stream: InputStream?, callback: ParsingDoneCallback) {
        feedItems = ArrayList()
        feedChannel
        try {
            val parser = XmlPullParserFactory.newInstance().newPullParser()
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            parser.setInput(stream, null)
            var eventType: Int
            var data = ""
            var title = ""
            var link = ""
            var description = ""
            var author = ""
            var guid = ""
            var pubDate = ""
            var mediaLink = ""
            var bitmap: Bitmap? = null
            var feed: FeedItem
            var channelTitle = ""
            var channelDescription = ""
            var channelLink = ""
            var channelLanguage = ""
            eventType = parser.eventType
            while (XmlPullParser.END_DOCUMENT != eventType) {
                val fieldName = parser.name
                when (eventType) {
                    XmlPullParser.START_TAG -> if (TAG_ITEM == fieldName || TAG_ENTRY == fieldName) {
                        if (isChannelArea) {
                            feedChannel = FeedChannel(channelTitle, channelTitle, channelLink,
                                    channelDescription, channelLanguage, rssUrl!!)
                        }
                        title = ""
                        link = ""
                        description = ""
                        author = ""
                        guid = ""
                        pubDate = ""
                        mediaLink = ""
                    }
                    XmlPullParser.TEXT -> data = parser.text
                    XmlPullParser.END_TAG -> if (TAG_ITEM == fieldName || TAG_ENTRY == fieldName) {
                        description = removeHtml(description)
                        feed = FeedItem(title, description, link, author,
                                guid, pubDate, mediaLink, bitmap!!, channelTitle)
                        feedItems.add(feed)
                    } else if (TAG_TITLE == fieldName) {
                        if (!isChannelArea) {
                            title = data
                        } else {
                            channelTitle = data
                        }
                    } else if (TAG_LINK == fieldName) {
                        if (!isChannelArea && "" == link) {
                            link = if (parser.attributeCount > 0) {
                                tryParseAtomLink(parser)
                            } else {
                                data
                            }
                        } else if (isChannelArea && "" == channelLink) {
                            channelLink = if (parser.attributeCount > 0) {
                                tryParseAtomLink(parser)
                            } else {
                                data
                            }
                        }
                    } else if (TAG_DESCRIPTION == fieldName || TAG_SUMMARY == fieldName) {
                        if (!isChannelArea) {
                            description = data
                        } else {
                            channelDescription = data
                        }
                    } else if (TAG_LANGUAGE == fieldName) {
                        if (isChannelArea) {
                            channelLanguage = data
                        }
                    } else if (TAG_PUB_DATE == fieldName || TAG_UPDATED == fieldName) {
                        pubDate = data
                    } else if (TAG_AUTHOR == fieldName) {
                        author = data
                    } else if (TAG_GUID == fieldName) {
                        guid = data
                    } else if (TAG_MEDIA_LINK == fieldName) {
                        val mediaDescription = parser.positionDescription
                        if (mediaDescription.contains(TAG_URL)) {
                            bitmap = downloadBitmap(mediaDescription)
                        }
                    }
                }
                eventType = parser.next()
            }
        } catch (e: IOException) {
            LOGGER.log(Level.WARNING, "Exception on working with input stream on XML parse")
        } catch (e: XmlPullParserException) {
            LOGGER.log(Level.WARNING, "Exception on working with XMLPullParser parse")
        }
        callback.onParsingDone()
    }

    @Throws(IOException::class)
    private fun downloadBitmap(mediaDescription: String): Bitmap? {
        val splitArray = mediaDescription.split("'").toTypedArray()
        val mediaLink = splitArray[1]
        val url: URL
        url = try {
            URL(mediaLink)
        } catch (e: MalformedURLException) {
            LOGGER.log(Level.WARNING, "$mediaLink : is not a url link")
            return null
        }
        val urlConnection = url.openConnection()
        urlConnection.readTimeout = READ_TIMEOUT_MS
        urlConnection.connectTimeout = CONNECT_TIMEOUT_MS
        return BitmapFactory.decodeStream(urlConnection.getInputStream())
    }

    private fun removeHtml(parsingResult: String): String {
        val result: String
        result = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(parsingResult, Html.FROM_HTML_MODE_LEGACY).toString().replace('\n', 32.toChar())
                    .replace(160.toChar(), 32.toChar()).replace(65532.toChar(), 32.toChar()).trim { it <= ' ' }
        } else {
            Html.fromHtml(parsingResult).toString().replace('\n', 32.toChar())
                    .replace(160.toChar(), 32.toChar()).replace(65532.toChar(), 32.toChar()).trim { it <= ' ' }
        }
        return result
    }

    private fun tryParseAtomLink(parser: XmlPullParser): String {
        val attributeCount = parser.attributeCount
        for (i in 0 until attributeCount) {
            val link = parser.getAttributeValue(i)
            if (link.contains(TAG_HTML)) {
                LOGGER.log(Level.INFO, link)
                return link
            }
        }
        return ""
    }

    private val isChannelArea: Boolean
        get() = feedChannel == null

    companion object {
        private const val READ_TIMEOUT_MS = 10000
        private const val CONNECT_TIMEOUT_MS = 15000
        private val LOGGER = Logger.getLogger(RSSFeedParser::class.java.name)
        private const val TAG_ITEM = "item"
        private const val TAG_TITLE = "title"
        private const val TAG_LINK = "link"
        private const val TAG_DESCRIPTION = "description"
        private const val TAG_LANGUAGE = "language"
        private const val TAG_AUTHOR = "dc:creator"
        private const val TAG_GUID = "guid"
        private const val TAG_PUB_DATE = "pubDate"
        private const val TAG_MEDIA_LINK = "media:content"
        private const val TAG_URL = "url"
        private const val TAG_ENTRY = "entry"
        private const val TAG_UPDATED = "updated"
        private const val TAG_SUMMARY = "summary"
        private const val TAG_HTML = ".html"
    }
}