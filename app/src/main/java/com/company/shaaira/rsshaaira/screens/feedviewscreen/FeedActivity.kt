package com.company.shaaira.rsshaaira.screens.feedviewscreen

import android.os.Bundle
import com.company.shaaira.rsshaaira.R
import com.company.shaaira.rsshaaira.screens.BaseLifecycleActivity

class FeedActivity : BaseLifecycleActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_feed)
        val feedView = FeedView(this)
        val feedController = FeedController(feedView)
        lifeCycleDependants.add(feedView)
        lifeCycleDependants.add(feedController)
        activityToolbarListeners.add(feedView)
        activityResultListeners.add(feedController)
        super.onCreate(savedInstanceState)
    }
}