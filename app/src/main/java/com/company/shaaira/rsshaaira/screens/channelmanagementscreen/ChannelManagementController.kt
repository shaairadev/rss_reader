package com.company.shaaira.rsshaaira.screens.channelmanagementscreen

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.os.SystemClock
import androidx.appcompat.app.AppCompatActivity
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.ActivityResultListener
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.DefaultLifecycleListener
import com.company.shaaira.rsshaaira.broadcasters.BroadcastPasser
import com.company.shaaira.rsshaaira.broadcasters.ReceiveListener
import com.company.shaaira.rsshaaira.scheduleupdater.AlarmReceiver.Companion.makeAlarmIntent
import com.company.shaaira.rsshaaira.screens.addchanneldialogue.AddChannelActivity
import com.company.shaaira.rsshaaira.screens.addchanneldialogue.AddChannelActivity.Companion.makeStartActivity
import com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks.*
import com.company.shaaira.rsshaaira.services.feedservice.RSSService
import com.company.shaaira.rsshaaira.services.feedservice.RSSService.RSSBinder
import com.company.shaaira.rsshaaira.services.feedservice.ServiceHandler
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.net.URL
import java.util.logging.Level
import java.util.logging.Logger

class ChannelManagementController(private val channelManagementView: ChannelManagementView) :
        DefaultLifecycleListener(), ActivityResultListener {

    private val channelManagementModel: ChannelManagementModel = ChannelManagementModel()
    private val serviceHandler: ServiceHandler = ServiceHandler()
    private val broadcastPasser: BroadcastPasser = BroadcastPasser()
    private var receiveListener: ReceiveListener? = null
    private lateinit var rssService: RSSService
    private var serviceBound = false
    private var queuedUrl: String = ""
    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            val binder = service as RSSBinder
            rssService = binder.service
            LOGGER.log(Level.INFO, "RSS service has been bound")
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            LOGGER.log(Level.INFO, "RSS service has been unbound")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        startReceiver()
        serviceHandler.startService(context, RSS_SERVICE_PATH)
        loadSavedData()
    }

    override fun onStart() {
        serviceHandler.bindRSSService(context, mConnection)
    }

    override fun onStop() {
        stopReceiver()
        serviceHandler.unbindRSSService(context, mConnection)
        serviceBound = false
    }

    private fun onChannelClickCallback(channelName: String?, url: URL?) {
        val sharedPreferences = context.getSharedPreferences(CURRENT_CHANNEL_TAG, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(CURRENT_CHANNEL_NAME, channelName)
        editor.putString(CURRENT_CHANNEL_URL, url.toString())
        editor.apply()
        (context as AppCompatActivity).setResult(Activity.RESULT_OK)
        (context as AppCompatActivity).finish()
    }

    private fun onAddChannelClickCallback() {
        val intent = makeStartActivity(context)
        (context as AppCompatActivity).startActivityForResult(intent, PASTE_RSS_URL_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PASTE_RSS_URL_REQUEST) {
            LOGGER.log(Level.INFO, "Request code success: $requestCode")
            if (resultCode == Activity.RESULT_OK) {
                if (!serviceBound) {
                    serviceHandler.bindRSSService(context, mConnection)
                    serviceBound = false
                }
                LOGGER.log(Level.INFO, "Result code success: $requestCode")
                queuedUrl = data?.getStringExtra(AddChannelActivity.URL_EXTRA_TAG) ?: ""
                rssService.fetchRSSFeed(queuedUrl)
            }
        }
    }

    private fun loadSavedData() {
        LOGGER.log(Level.INFO, "Loading channel data from internal storage")
        val fileInputStream: FileInputStream = try {
            context.openFileInput(CHANNELS_FILE_NAME)
        } catch (e: FileNotFoundException) {
            //TODO:Оповестить юзера об отсутствии записанных данных и считать стандартные данные из библиотеки
            LOGGER.log(Level.WARNING, "File was not found : $CHANNELS_FILE_NAME")
            return
        }
        channelManagementModel.loadChannelDataInternal(fileInputStream, object : ChannelDataReadCallback {
            override fun onReady() {
                channelManagementView.updateRecyclerView(channelManagementModel.feedChannels)
                try {
                    fileInputStream.close()
                } catch (e: IOException) {
                    LOGGER.log(Level.WARNING, "Attempt to close the closed stream")
                }
            }
        })
    }

    private fun startReceiver() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(RSSService.actionFilter)
        receiveListener = makeReceiveListener()
        broadcastPasser.addBroadcastListener(receiveListener!!)
        context.registerReceiver(broadcastPasser, intentFilter)
        LOGGER.log(Level.INFO, "Management receiver has been registered")
    }

    private fun stopReceiver() {
        broadcastPasser.removeBroadcastListener(receiveListener!!)
        context.unregisterReceiver(broadcastPasser)
        LOGGER.log(Level.INFO, "Management receiver has been unregistered")
    }

    private fun makeReceiveListener(): ReceiveListener {
        return object : ReceiveListener {
            override fun onReceiveCallback(context: Context, intent: Intent) {
                LOGGER.log(Level.INFO, "Working with callback of broadcast received")
                val channelName = intent.getStringExtra(RSSService.intentTag)
                val channel = rssService.getParsedChannel(queuedUrl)
                if (channel != null) {
                    if (channelManagementModel.checkUrlTaken(channel.rssLink.toString())) {
                        return
                    }
                    try {
                        channelManagementModel.addNewChannel(context.openFileOutput(
                                CHANNELS_FILE_NAME, Context.MODE_PRIVATE), channel.feedChannel)
                        channelManagementView.updateRecyclerView(channel.feedChannel)
                        rssService.removeParsedChannel(queuedUrl)
                        setOneHourUpdate(context, queuedUrl)
                    } catch (e: FileNotFoundException) {
                        LOGGER.log(Level.WARNING, "File was not found $channelName$CHANNELS_FILE_NAME")
                    }
                }
            }
        }
    }

    private fun setOneHourUpdate(context: Context, scheduledChannel: String?) {
        val alarmMgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = makeAlarmIntent(context, scheduledChannel)
        val alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_HOUR,
                AlarmManager.INTERVAL_HOUR, alarmIntent)
    }

    private val context: Context
        get() = channelManagementView.context

    companion object {
        private val LOGGER = Logger.getLogger(ChannelManagementController::class.java.name)
        private const val CURRENT_CHANNEL_TAG = "shared_options_tag"
        private const val CURRENT_CHANNEL_NAME = "name_current_channel"
        private const val CURRENT_CHANNEL_URL = "url_current_channel"
        private const val CHANNELS_FILE_NAME = "channels_data_saved"
        private const val RSS_SERVICE_PATH = "com.company.shaaira.rsshaaira.services.rss_service.RSSService"
        private const val PASTE_RSS_URL_REQUEST = 11199
    }

    init {
        this.channelManagementView.setAddChannelClickCallback(object : AddChannelClickCallback {
            override fun onClick() {
                onAddChannelClickCallback()
            }
        })

        this.channelManagementView.setChannelClickCallback(object : ChannelItemClickCallback {
            override fun onClick(itemTitle: String?, url: URL?) {
                onChannelClickCallback(itemTitle, url)
            }
        })

        this.channelManagementView.setChannelDeleteCallback(object : ChannelDeleteCallback {
            override fun onDelete(itemTitle: String?) {
                try {
                    if (channelManagementModel.deleteChannel(context.openFileOutput(
                                    CHANNELS_FILE_NAME, Context.MODE_PRIVATE), itemTitle)) {
                        channelManagementView.updateRecyclerView(channelManagementModel.feedChannels)
                    }
                } catch (e: FileNotFoundException) {
                    LOGGER.log(Level.WARNING, "Attempt to delete channel when no channels were saved at all")
                }
                LOGGER.log(Level.INFO, "Menu item 'delete' was clicked")
            }
        })

        this.channelManagementView.setChannelEditCallback(object : ChannelEditCallback {
            override fun onEdit(title: String?) {
                //TODO: Реализовать логику редактирования канала
                LOGGER.log(Level.INFO, "Menu item 'edit' was clicked")
            }
        })
    }
}