package com.company.shaaira.rsshaaira.feeddata

import android.graphics.Bitmap


data class FeedItem(
        val title: String,
        val description: String,
        val sourceLink: String,
        val author: String,
        val guid: String,
        val pubDate: String,
        val mediaLink: String,
        val bitmap: Bitmap?,
        val channel: String
)