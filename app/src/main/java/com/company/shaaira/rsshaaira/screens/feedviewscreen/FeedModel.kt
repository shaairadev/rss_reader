package com.company.shaaira.rsshaaira.screens.feedviewscreen

import android.content.Context
import com.company.shaaira.rsshaaira.feeddata.FeedItem
import com.company.shaaira.rsshaaira.services.feedservice.callbacks.RSSReadyCallback
import com.company.shaaira.rsshaaira.services.feedservice.datamodules.DatabaseStorageManager
import com.company.shaaira.rsshaaira.services.feedservice.datamodules.InnerStorageDataManager
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

internal class FeedModel(context: Context) {
    private val innerStorageDataManager: InnerStorageDataManager = InnerStorageDataManager()
    private val databaseStorageManager: DatabaseStorageManager = DatabaseStorageManager(context)
    private val rssFeed = HashMap<String, ArrayList<FeedItem>?>()
    fun getFeed(channelName: String?): ArrayList<FeedItem>? {
        return rssFeed[channelName]
    }

    fun instantiateDatabase() {
        databaseStorageManager.instantiateDatabase()
    }

    fun closeDatabase() {
        databaseStorageManager.closeDatabase()
    }

    fun addNewChannelItems(channelName: String,
                           feedItems: ArrayList<FeedItem>?) {
        rssFeed[channelName] = feedItems
    }

    @Deprecated("")
    fun saveDataInternal(fileOutputStream: FileOutputStream?, channelName: String?) {
        val channel = rssFeed[channelName] ?: return
        val writeThread = Thread(Runnable {
            try {
                innerStorageDataManager.saveFeedData(fileOutputStream!!, channel)
            } catch (e: IOException) {
                LOGGER.log(Level.WARNING, "Wrong input stream")
            }
        })
        writeThread.start()
    }

    @Deprecated("")
    fun loadDataInternal(channelName: String, fileInputStream: FileInputStream?, callback: RSSReadyCallback) {
        val loadThread = Thread(Runnable {
            innerStorageDataManager.readSavedFeedData(fileInputStream, object : RSSReadyCallback {
                override fun onReady(result: Boolean) {
                    if (result) {
                        rssFeed[channelName] = innerStorageDataManager.feedItems
                    }
                    callback.onReady(result)
                }
            })
        })
        loadThread.start()
    }

    fun saveToDatabase(channelName: String?) {
        val channel = rssFeed[channelName] ?: return
        val databaseThread = Thread(Runnable { databaseStorageManager.saveFeedData(channel) })
        databaseThread.start()
    }

    fun loadFromDatabase(channelName: String, callback: RSSReadyCallback) {
        LOGGER.log(Level.INFO, "Starting reading database storage")
        val databaseThread = Thread(Runnable {
            databaseStorageManager.readSavedFeedData(channelName, object : RSSReadyCallback {
                override fun onReady(result: Boolean) {
                    if (result) {
                        rssFeed[channelName] = databaseStorageManager.feedItems
                    }
                    LOGGER.log(Level.INFO, "Result of reading database storage : $result")
                    callback.onReady(result)
                }
            })
        })
        databaseThread.start()
    }

    companion object {
        private val LOGGER = Logger.getLogger(FeedModel::class.java.name)
    }

}