package com.company.shaaira.rsshaaira.services.feedservice.datamodules

import android.content.ContentValues
import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import com.company.shaaira.rsshaaira.database.FeedDatabaseHelper
import com.company.shaaira.rsshaaira.database.FeedEntryInfo
import com.company.shaaira.rsshaaira.feeddata.FeedItem
import com.company.shaaira.rsshaaira.feeddata.FeedItemSerializable
import com.company.shaaira.rsshaaira.services.feedservice.callbacks.RSSReadyCallback
import java.net.MalformedURLException
import java.util.logging.Level
import java.util.logging.Logger

class DatabaseStorageManager(context: Context) {
    private val feedDatabaseHelper: FeedDatabaseHelper = FeedDatabaseHelper(context)
    private var readDatabase: SQLiteDatabase? = null
    private var writeDatabase: SQLiteDatabase? = null
    var feedItems: ArrayList<FeedItem> = ArrayList()
        private set

    fun instantiateDatabase() {
        if (readDatabase == null || !readDatabase!!.isOpen) {
            readDatabase = feedDatabaseHelper.readableDatabase
        }
        if (writeDatabase == null || !writeDatabase!!.isOpen) {
            writeDatabase = feedDatabaseHelper.writableDatabase
        }
    }

    fun closeDatabase() {
        if (readDatabase == null || writeDatabase == null) {
            return
        }
        readDatabase!!.close()
        writeDatabase!!.close()
    }

    fun saveFeedData(newFeedItems: ArrayList<FeedItem>?) {
        LOGGER.log(Level.INFO, "Saving feed data to SQL storage")
        if (newFeedItems == null || newFeedItems.size == 0) {
            LOGGER.log(Level.WARNING, "Attempt to save empty data list. Abort")
            return
        }
        val valuesList = ArrayList<ContentValues>()
        val mergedItems = mergeUniqueData(newFeedItems[0].channel, newFeedItems)
        for (item in mergedItems) {
            val serializedItem = FeedItemSerializable.serialize(item)
            val values = ContentValues()
            values.put(FeedEntryInfo.COLUMN_TITLE, serializedItem.title)
            values.put(FeedEntryInfo.COLUMN_DESCRIPTION, serializedItem.description)
            values.put(FeedEntryInfo.COLUMN_AUTHOR, serializedItem.author)
            values.put(FeedEntryInfo.COLUMN_GUID, serializedItem.guid)
            values.put(FeedEntryInfo.COLUMN_PUBDATE, serializedItem.pubDate)
            values.put(FeedEntryInfo.COLUMN_BITMAP, serializedItem.encodedBitmap)
            values.put(FeedEntryInfo.COLUMN_MEDIA_LINK, serializedItem.mediaLink.toString())
            values.put(FeedEntryInfo.COLUMN_SOURCE_LINK, serializedItem.sourceLink.toString())
            values.put(FeedEntryInfo.COLUMN_CHANNEL, serializedItem.channel)
            valuesList.add(values)
        }
        try {
            writeDatabase!!.beginTransaction()
        } catch (e: NullPointerException) {
            LOGGER.log(Level.WARNING, "Aborting attempt of save. Database is not initialized")
            return
        }
        try {
            for (item in valuesList) {
                try {
                    writeDatabase!!.insert(FeedEntryInfo.TABLE_NAME, null, item)
                } catch (ignored: SQLiteConstraintException) {
                }
            }
            writeDatabase!!.setTransactionSuccessful()
        } catch (e: SQLException) {
            LOGGER.log(Level.INFO, "Error saving to database")
        } finally {
            writeDatabase!!.endTransaction()
            LOGGER.log(Level.INFO, "Data saved to database")
        }
    }

    fun readSavedFeedData(channel: String, callback: RSSReadyCallback) {
        val selectionArgs = arrayOf(channel)
        try {
            val cursor = readDatabase!!.query(
                    FeedEntryInfo.TABLE_NAME,
                    PROJECTION,
                    SELECTION,
                    selectionArgs,
                    null,
                    null,
                    "$SORTING_ORDER DESC"
            )
            feedItems = ArrayList()
            while (cursor.moveToNext()) {
                try {
                    feedItems.add(FeedItem(cursor.getString(cursor.getColumnIndex(FeedEntryInfo.COLUMN_TITLE)),
                            cursor.getString(cursor.getColumnIndex(FeedEntryInfo.COLUMN_DESCRIPTION)),
                            cursor.getString(cursor.getColumnIndex(FeedEntryInfo.COLUMN_SOURCE_LINK)),
                            cursor.getString(cursor.getColumnIndex(FeedEntryInfo.COLUMN_AUTHOR)),
                            cursor.getString(cursor.getColumnIndex(FeedEntryInfo.COLUMN_GUID)),
                            cursor.getString(cursor.getColumnIndex(FeedEntryInfo.COLUMN_PUBDATE)),
                            cursor.getString(cursor.getColumnIndex(FeedEntryInfo.COLUMN_MEDIA_LINK)),
                            BitmapDataSerializer.deserializeBitmap(cursor.getBlob(cursor.getColumnIndex(FeedEntryInfo.COLUMN_BITMAP))),
                            cursor.getString(cursor.getColumnIndex(FeedEntryInfo.COLUMN_CHANNEL))
                    ))
                } catch (e: MalformedURLException) {
                    LOGGER.log(Level.WARNING, "Error on creating URL")
                    callback.onReady(false)
                    return
                }
            }
            cursor.close()
            if (feedItems.size == 0) {
                LOGGER.log(Level.INFO, "Database entry is empty")
                callback.onReady(false)
            } else {
                callback.onReady(true)
            }
        } catch (e: NullPointerException) {
            LOGGER.log(Level.WARNING, "Database is not initialized. Initializing")
            instantiateDatabase()
            readSavedFeedData(channel, callback)
        } catch (e: IllegalStateException) {
            LOGGER.log(Level.WARNING, "Database is not initialized. Initializing")
            instantiateDatabase()
            readSavedFeedData(channel, callback)
        }
    }

    private fun mergeUniqueData(channel: String, newList: ArrayList<FeedItem>): ArrayList<FeedItem> {
        val mergedList = ArrayList<FeedItem>()
        readSavedFeedData(channel, object : RSSReadyCallback {
            override fun onReady(result: Boolean) {
                val savedList = ArrayList(feedItems)
                for (i in newList.indices) {
                    var containing = false
                    for (j in 0 until savedList.size) {
                        if (compareItems(savedList[j], newList[i])) {
                            containing = true
                        }
                    }
                    if (!containing) {
                        mergedList.add(newList[i])
                    }
                }
            }
        })
        return mergedList
    }

    private fun compareItems(first: FeedItem, second: FeedItem): Boolean {
        return first.title == second.title
    }

    companion object {
        //TODO: Добавить обработки SQL Exception-ов
        private val LOGGER = Logger.getLogger(DatabaseStorageManager::class.java.name)
        private val PROJECTION = arrayOf(
                FeedEntryInfo.COLUMN_TITLE,
                FeedEntryInfo.COLUMN_DESCRIPTION,
                FeedEntryInfo.COLUMN_AUTHOR,
                FeedEntryInfo.COLUMN_GUID,
                FeedEntryInfo.COLUMN_PUBDATE,
                FeedEntryInfo.COLUMN_BITMAP,
                FeedEntryInfo.COLUMN_MEDIA_LINK,
                FeedEntryInfo.COLUMN_SOURCE_LINK,
                FeedEntryInfo.COLUMN_CHANNEL
        )
        private const val SELECTION = FeedEntryInfo.COLUMN_CHANNEL + " = ?"
        private const val SORTING_ORDER = FeedEntryInfo.COLUMN_PUBDATE
    }

}