package com.company.shaaira.rsshaaira.screens.addchanneldialogue

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.company.shaaira.rsshaaira.R

class AddChannelActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_channel)
        val fetchInputButton = findViewById<Button>(R.id.fetchInputButton)
        fetchInputButton.setOnClickListener {
            val data = Intent()
            val editText = findViewById<EditText>(R.id.userURLInputText)
            val text = editText.text.toString()
            data.putExtra(URL_EXTRA_TAG, text)
            setResult(RESULT_OK, data)
            finish()
        }
    }

    companion object {
        const val URL_EXTRA_TAG = "url_tag_extra"
        fun makeStartActivity(context: Context): Intent {
            return Intent(context, AddChannelActivity::class.java)
        }
    }
}