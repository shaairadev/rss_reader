package com.company.shaaira.rsshaaira.screens.feedviewscreen.feedcontrollermodules

import android.text.format.DateUtils
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

class FeedDateFormatter {
    private val rfcDateFormatter: DateFormat
    private val feedDateFormatter: DateFormat

    private fun getDataObject(pubDate: String): Calendar {
        return try {
            val date = rfcDateFormatter.parse(pubDate) ?: Date()
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar
        } catch (e: ParseException) {
            LOGGER.log(Level.WARNING, "Wrong pub date format")
            Calendar.getInstance()
        }
    }

    private fun getFormattedDate(date: Calendar): String {
        return feedDateFormatter.format(date.time)
    }

    fun getFormattedDate(pubDate: String): String {
        val date = getDataObject(pubDate)
        return getFormattedDate(date)
    }

    private fun getRelativeDay(date: Date): String {
        val now = Calendar.getInstance()
        val then = Calendar.getInstance()
        then.time = date
        return DateUtils.getRelativeTimeSpanString(
                then.timeInMillis,
                now.timeInMillis,
                DateUtils.DAY_IN_MILLIS,
                DateUtils.FORMAT_SHOW_WEEKDAY).toString()
    }

    private val weekDay: String
        get() {
            val dayFormat = SimpleDateFormat(DAY_FORMAT, Locale.ENGLISH)
            return dayFormat.format(Calendar.getInstance().timeInMillis)
        }

    companion object {
        private val LOGGER = Logger.getLogger(FeedDateFormatter::class.java.name)
        private const val RFC_FORMAT = "EEE, dd MMM yyyy HH:mm:ss Z"
        private const val DAY_FORMAT = "EEEE"
        private const val FEED_FORMAT = "dd.MM.yyyy, HH:mm"
    }

    init {
        rfcDateFormatter = SimpleDateFormat(RFC_FORMAT, Locale.ENGLISH)
        feedDateFormatter = SimpleDateFormat(FEED_FORMAT, Locale.ENGLISH)
    }
}