package com.company.shaaira.rsshaaira.services.feedservice.callbacks

interface RSSReadyCallback {
    fun onReady(result: Boolean)
}