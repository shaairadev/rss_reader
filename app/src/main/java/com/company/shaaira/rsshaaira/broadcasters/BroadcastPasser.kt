package com.company.shaaira.rsshaaira.broadcasters

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

class BroadcastPasser : BroadcastReceiver() {
    private val mReceiveListeners = ArrayList<ReceiveListener>()
    fun addBroadcastListener(receiveListener: ReceiveListener) {
        mReceiveListeners.add(receiveListener)
    }

    fun removeBroadcastListener(receiveListener: ReceiveListener) {
        if (mReceiveListeners.contains(receiveListener)) {
            mReceiveListeners.remove(receiveListener)
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        for (listener in mReceiveListeners) {
            LOGGER.log(Level.INFO, "Broadcast passer is passing receive message to messenger")
            listener.onReceiveCallback(context, intent)
        }
    }

    companion object {
        private val LOGGER = Logger.getLogger(BroadcastPasser.toString())
    }
}