package com.company.shaaira.rsshaaira.services.feedservice

import android.app.ActivityManager
import android.content.Context
import android.content.ServiceConnection

class ServiceHandler {
    private fun isServiceRunning(context: Context, servicePath: String): Boolean {
        val manager = (context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
        if (manager.getRunningServices(Int.MAX_VALUE).size < 0) {
            return false
        }
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (service.service.className.contentEquals(servicePath)) {
                return true
            }
        }
        return false
    }

    fun startService(context: Context, servicePath: String) {
        if (isServiceRunning(context, servicePath)) {
            return
        }
        val intent = RSSService.makeIntent(context)
        context.startService(intent)
    }

    fun bindRSSService(context: Context, connection: ServiceConnection) {
        val intent = RSSService.makeIntent(context)
        context.bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }

    fun unbindRSSService(context: Context, connection: ServiceConnection) {
        context.unbindService(connection)
    }
}