package com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks

interface AddChannelClickCallback {
    fun onClick()
}