package com.company.shaaira.rsshaaira.screens.feedviewscreen

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.company.shaaira.rsshaaira.R
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.ActivityToolbarListener
import com.company.shaaira.rsshaaira.activitycallbackinterfaces.DefaultLifecycleListener
import com.company.shaaira.rsshaaira.feeddata.FeedItem
import com.company.shaaira.rsshaaira.screens.feedviewscreen.adapters.FeedAdapter
import com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks.ChannelMenuCallback
import com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks.FeedItemClickCallback
import com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks.RefreshCallback
import java.util.*

class FeedView internal constructor(private val feedActivity: AppCompatActivity)
    : DefaultLifecycleListener(), ActivityToolbarListener {

    private lateinit var feedAdapter: FeedAdapter
    private var topToolbar: Toolbar? = null
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var refreshCallback: RefreshCallback? = null
    private var channelMenuCallback: ChannelMenuCallback? = null
    private var feedClickCallback: FeedItemClickCallback? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        feedAdapter = initRecyclerView()
        topToolbar = feedActivity.findViewById(R.id.feed_view_menu)
        setToolbarTitle(feedActivity.getString(R.string.title))
        feedActivity.setSupportActionBar(topToolbar)
        swipeRefreshLayout = feedActivity.findViewById(R.id.swipe_layout)
        swipeRefreshLayout.setOnRefreshListener {
            if (refreshCallback != null) {
                refreshCallback!!.onRefresh()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (R.id.menu_refresh == item.itemId) {
            if (refreshCallback != null) {
                setRefreshAnimation(true)
                refreshCallback!!.onRefresh()
            }
        } else if (R.id.menu_channels == item.itemId) {
            if (channelMenuCallback != null) {
                channelMenuCallback!!.onClick()
            }
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        inflateMenu(menu)
        return true
    }

    fun setChannelMenuCallback(channelMenuCallback: ChannelMenuCallback?) {
        this.channelMenuCallback = channelMenuCallback
    }

    fun setRefreshCallback(refreshCallback: RefreshCallback?) {
        this.refreshCallback = refreshCallback
    }

    fun setFeedClickCallback(feedClickCallback: FeedItemClickCallback?) {
        this.feedClickCallback = feedClickCallback
    }

    fun updateRecyclerView(feedItems: ArrayList<FeedItem>) {
        feedActivity.runOnUiThread {
            feedAdapter.updateAdapterData(feedItems)
            setRefreshAnimation(false)
        }
    }

    fun setRefreshAnimation(state: Boolean) {
        feedActivity.runOnUiThread {
            if (swipeRefreshLayout.isRefreshing != state) {
                swipeRefreshLayout.isRefreshing = state
            }
        }
    }

    fun setToolbarTitle(title: String?) {
        feedActivity.runOnUiThread(Runnable {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if ("" == title || title == null) {
                    return@Runnable
                }
                topToolbar!!.title = title
            }
        })
    }

    val context: Context
        get() = feedActivity

    private fun inflateMenu(menu: Menu) {
        feedActivity.menuInflater.inflate(R.menu.feed_toolbar_menu, menu)
    }

    private fun initRecyclerView(): FeedAdapter {
        val recyclerView = feedActivity.findViewById(R.id.feed_view_recyclerView) as RecyclerView
        val adapter = FeedAdapter(ArrayList(), object : FeedItemClickCallback {
            override fun onClick(itemData: String) {
                feedClickCallback!!.onClick(itemData)
            }
        })
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(feedActivity)
        return adapter
    }

    init {

    }
}