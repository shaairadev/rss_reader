package com.company.shaaira.rsshaaira.services.feedservice.datamodules

import com.company.shaaira.rsshaaira.services.feedservice.callbacks.DataDownloadedCallback
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.logging.Level
import java.util.logging.Logger

class XMLDownloader {
    var rssDownloadResult: InputStream? = null
        private set

    fun downloadRSSFeed(url: URL, callback: DataDownloadedCallback) {
        try {
            val urlConnection = url.openConnection() as HttpURLConnection
            urlConnection.readTimeout = READ_TIMEOUT_MS
            urlConnection.connectTimeout = CONNECT_TIMEOUT_MS
            urlConnection.requestMethod = "GET"
            urlConnection.doInput = true
            urlConnection.connect()
            LOGGER.log(Level.INFO, "Url connection has been done")
            rssDownloadResult = urlConnection.inputStream
            callback.onDownload(true)
        } catch (e: Exception) {
            callback.onDownload(false)
        }
    }

    fun closeDownloadStream() {
        try {
            rssDownloadResult!!.close()
        } catch (e: IOException) {
            LOGGER.log(Level.WARNING, "Attempt closing the closed input stream")
        }
    }

    companion object {
        private val LOGGER = Logger.getLogger(XMLDownloader::class.java.name)
        private const val READ_TIMEOUT_MS = 10000
        private const val CONNECT_TIMEOUT_MS = 15000
    }
}