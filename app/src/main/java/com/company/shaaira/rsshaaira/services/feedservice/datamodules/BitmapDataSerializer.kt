package com.company.shaaira.rsshaaira.services.feedservice.datamodules

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream
import java.io.Serializable

object BitmapDataSerializer : Serializable {
    private const val BEST_QUALITY = 100
    private const val DEFAULT_OFFSET = 0

    fun serializeBitmap(bitmap: Bitmap?): ByteArray {
        val stream = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.PNG, BEST_QUALITY, stream)
        return stream.toByteArray()
    }


    fun deserializeBitmap(encodedBitmap: ByteArray): Bitmap? {
        return if (encodedBitmap.isEmpty()) {
            null
        } else BitmapFactory.decodeByteArray(encodedBitmap, DEFAULT_OFFSET, encodedBitmap.size)
    }
}