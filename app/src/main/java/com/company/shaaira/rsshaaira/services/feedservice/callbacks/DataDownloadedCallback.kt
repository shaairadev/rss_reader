package com.company.shaaira.rsshaaira.services.feedservice.callbacks

interface DataDownloadedCallback {
    fun onDownload(success: Boolean)
}