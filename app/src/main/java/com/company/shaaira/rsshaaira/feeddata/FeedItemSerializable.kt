package com.company.shaaira.rsshaaira.feeddata

import com.company.shaaira.rsshaaira.services.feedservice.datamodules.BitmapDataSerializer
import java.net.URL

data class FeedItemSerializable(
        val title: String,
        val description: String,
        val sourceLink: URL,
        val author: String,
        val guid: String,
        val pubDate: String,
        val mediaLink: URL,
        val encodedBitmap: ByteArray,
        val channel: String
) {
    companion object {
        fun deserialize(serializedItem: FeedItemSerializable): FeedItem {
            return FeedItem(serializedItem.title, serializedItem.description, serializedItem.sourceLink.toString(),
                    serializedItem.author, serializedItem.guid, serializedItem.pubDate,
                    serializedItem.mediaLink.toString(), BitmapDataSerializer.deserializeBitmap(serializedItem.encodedBitmap),
                    serializedItem.channel)
        }


        fun serialize(feedItem: FeedItem): FeedItemSerializable {
            return FeedItemSerializable(feedItem.title, feedItem.description, URL(feedItem.sourceLink),
                    feedItem.author, feedItem.guid, feedItem.pubDate,
                    URL(feedItem.mediaLink), BitmapDataSerializer.serializeBitmap(feedItem.bitmap),
                    feedItem.channel)
        }
    }
}

