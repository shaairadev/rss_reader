package com.company.shaaira.rsshaaira.screens.feedviewscreen.callbacks

interface ChannelMenuCallback {
    fun onClick()
}