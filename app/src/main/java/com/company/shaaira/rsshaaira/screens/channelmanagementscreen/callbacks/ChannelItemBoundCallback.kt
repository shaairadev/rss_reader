package com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks

import android.view.View

interface ChannelItemBoundCallback {
    fun onItemBound(view: View?, title: String?)
}