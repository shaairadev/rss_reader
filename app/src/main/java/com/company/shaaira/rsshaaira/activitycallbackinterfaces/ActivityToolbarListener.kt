package com.company.shaaira.rsshaaira.activitycallbackinterfaces

import android.view.Menu
import android.view.MenuItem

interface ActivityToolbarListener {
    fun onOptionsItemSelected(item: MenuItem): Boolean
    fun onCreateOptionsMenu(menu: Menu): Boolean
}