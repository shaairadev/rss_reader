package com.company.shaaira.rsshaaira.services.feedservice.callbacks

interface ParsingDoneCallback {
    fun onParsingDone()
}