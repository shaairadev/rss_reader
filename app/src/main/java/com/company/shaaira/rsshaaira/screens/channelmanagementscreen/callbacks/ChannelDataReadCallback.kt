package com.company.shaaira.rsshaaira.screens.channelmanagementscreen.callbacks

interface ChannelDataReadCallback {
    fun onReady()
}