package com.company.shaaira.rsshaaira.broadcasters

import android.content.Context
import android.content.Intent

interface ReceiveListener {
    fun onReceiveCallback(context: Context, intent: Intent)
}