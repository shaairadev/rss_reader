package com.company.shaaira.rsshaaira.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class FeedDatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES)
        onCreate(sqLiteDatabase)
    }

    override fun onDowngrade(sqLiteDatabase: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(sqLiteDatabase, oldVersion, newVersion)
    }

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "feed_database"
        private const val SQL_CREATE_ENTRIES = ("CREATE TABLE " + FeedEntryInfo.TABLE_NAME + "("
                + FeedEntryInfo.COLUMN_TITLE + " TEXT PRIMARY KEY,"
                + FeedEntryInfo.COLUMN_DESCRIPTION + " TEXT,"
                + FeedEntryInfo.COLUMN_AUTHOR + " TEXT,"
                + FeedEntryInfo.COLUMN_GUID + " TEXT,"
                + FeedEntryInfo.COLUMN_PUBDATE + " TEXT,"
                + FeedEntryInfo.COLUMN_BITMAP + " BLOB,"
                + FeedEntryInfo.COLUMN_MEDIA_LINK + " TEXT,"
                + FeedEntryInfo.COLUMN_SOURCE_LINK + " TEXT,"
                + FeedEntryInfo.COLUMN_CHANNEL + " TEXT"
                + ")")
        private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + FeedEntryInfo.TABLE_NAME
    }
}